import { DefaultSeoProps } from 'next-seo';

const SEO: DefaultSeoProps = {
  defaultTitle: 'JEBT 💾 Computer Science Engineer',
  titleTemplate: '%s 💾 Computer Science Engineer',
  description: 'Personal Website',
  canonical: 'https://futura64.com/',
  additionalMetaTags: [
    {
      name: 'author',
      content: 'JEBT',
    },
    {
      name: 'author',
      content: '_darmane',
    },
    {
      name: 'twitter:image',
      content: '/icon.png',
    },
    {
      property: 'og:image',
      content: '/icon.png',
    },
  ],
  additionalLinkTags: [
    {
      rel: 'icon',
      href: '/favicon.ico',
    },
    {
      rel: 'apple-touch-icon',
      href: '/icon.png',
      sizes: '76x76',
    },
    {
      rel: 'shortcut icon',
      href: '/favicon.ico',
      type: 'image/x-icon',
    },
  ],
  twitter: {
    cardType: 'summary_large_image',
    site: '@_darmane',
    handle: '@_darmane',
  },
  openGraph: {
    url: 'https://futura64.com',
    type: 'website',
    locale: 'en_US',
    // eslint-disable-next-line camelcase
    site_name: 'JEBT 💾 Computer Science Engineer',
    title: 'JEBT 💾 Computer Science Engineer',
    description: 'Personal Website',
  },
};

export default SEO;
