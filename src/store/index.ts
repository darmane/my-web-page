import create from 'zustand';
import { persist, devtools } from 'zustand/middleware';

interface CookiesState {
  accepted: boolean;
  accept: () => void;
}

export const useStore = create<CookiesState>()(
  devtools(
    persist(
      set => ({
        accepted: false,
        accept: () => set(state => ({ accepted: true })),
      }),
      {
        name: 'cookies-storage',
      }
    )
  )
);
