import PageSection from '@/components/page-section';
import {
  Heading,
  Text,
  VStack,
  Tabs,
  Tab,
  TabList,
  TabPanels,
  TabPanel,
} from '@chakra-ui/react';
import resume, { IMilestone } from '@/lib/resume';
import InlineLink from '@/components/inline-link';

const Milestone = ({ title, place, date, description, skills }: IMilestone) => {
  return (
    <VStack alignItems='start' spacing='2'>
      <VStack alignItems='start' spacing={0}>
        <Text fontWeight='bold'>{title}</Text>
        <Text>{place}</Text>
        <Text>{date}</Text>
      </VStack>
      <Text>{description}</Text>
      {skills && (
        <Text>
          <Text as='span' fontWeight='bold'>
            &bull; Tech:
          </Text>{' '}
          {skills}
        </Text>
      )}
    </VStack>
  );
};

const Resume = () => {
  return (
    <PageSection>
      <Heading as='h2' size='lg' marginBottom={8}>
        Resume
      </Heading>
      <VStack gap={0}>
        <Text width='full'>
          Please, <InlineLink href='/contact'>🧑‍💻contact me</InlineLink> if you
          want a PDF copy of my resume!
        </Text>
        <Tabs isFitted variant='line' width='full' colorScheme='teal'>
          <TabList>
            <Tab>Experience</Tab>
            <Tab>Education</Tab>
          </TabList>
          <TabPanels>
            <TabPanel
              as={VStack}
              spacing={{ base: '6', sm: '4' }}
              alignItems='start'
              width='full'
              paddingX='0'
              paddingBottom='0'
            >
              {resume.experience.map((milestone, index) => (
                <Milestone
                  key={index}
                  {...milestone}
                  title={milestone.place}
                  place={milestone.title}
                />
              ))}
            </TabPanel>
            <TabPanel
              as={VStack}
              spacing={{ base: '6', sm: '4' }}
              alignItems='start'
              width='full'
              paddingX='0'
              paddingBottom='0'
            >
              {resume.education.map((milestone, index) => (
                <Milestone key={index} {...milestone} />
              ))}
            </TabPanel>
          </TabPanels>
        </Tabs>
      </VStack>
    </PageSection>
  );
};

export default Resume;
