import {
  Flex,
  Heading,
  Icon,
  IconButton,
  Text,
  useColorModeValue,
  chakra,
  shouldForwardProp,
} from '@chakra-ui/react';
import { motion, isValidMotionProp } from 'framer-motion';
import { IconType } from 'react-icons/lib';
import PageSection from '@/components/page-section';
// Icons
import { FaBookReader, FaBrain, FaPlug } from 'react-icons/fa';
import {
  RiLightbulbFill,
  RiTeamFill,
  RiGroupFill,
  RiTimeFill,
} from 'react-icons/ri';
import { GiPublicSpeaker, GiTeamIdea, GiMarsCuriosity } from 'react-icons/gi';
import { BsGlobe, BsAppIndicator, BsGraphUp } from 'react-icons/bs';
import { HiArrowPath } from 'react-icons/hi2';

const ANIM_TIME = 0.5;

const SKILLS = [
  { title: 'Continuous learning', color: 'pink', icon: FaBookReader },
  { title: 'Problem Solving', color: 'orange', icon: RiLightbulbFill },
  { title: 'Communication', color: 'yellow', icon: GiPublicSpeaker },
  { title: 'Critical Thinking', color: 'green', icon: FaBrain },
  { title: 'Teamwork', color: 'teal', icon: RiTeamFill },
  { title: 'Cultural Competency', color: 'blue', icon: RiGroupFill },
  { title: 'Time Management', color: 'cyan', icon: RiTimeFill },
  { title: 'Adaptability', color: 'purple', icon: FaPlug },
  { title: 'Design patterns', color: 'pink', icon: BsAppIndicator },
  { title: 'Curiosity', color: 'orange', icon: GiMarsCuriosity },
  { title: 'Self-Management', color: 'yellow', icon: GiTeamIdea },
  { title: 'SOLID', color: 'green', icon: BsGlobe },
  { title: 'Agile', color: 'teal', icon: HiArrowPath },
  { title: 'Team-Management', color: 'blue', icon: BsGraphUp },
];

const SkillContainer = chakra(motion.div, {
  shouldForwardProp: prop => isValidMotionProp(prop) || shouldForwardProp(prop),
});

const Skill = ({
  skill,
  delay,
}: {
  skill: { title: string; color: string; icon: IconType };
  delay: number;
}) => {
  return (
    <SkillContainer
      initial={{
        opacity: 0,
        x: 100,
      }}
      whileInView={{
        opacity: 1,
        x: 0,
        transition: {
          delay: delay,
          type: 'spring',
          damping: 20,
        },
      }}
      viewport={{
        once: true,
      }}
      width={{ base: 'full', sm: 'unset' }}
    >
      <Flex
        padding={4}
        gap={4}
        bg={useColorModeValue('white', 'blackAlpha.700')}
        boxShadow={'xl'}
        rounded={'lg'}
        alignItems='center'
        height='fit-content'
        border='solid'
        borderColor={skill.color}
      >
        <IconButton
          icon={<Icon as={skill.icon} />}
          aria-label={skill.title}
          rounded='full'
          colorScheme={skill.color}
          size='sm'
          _hover={{}}
          _active={{}}
          cursor='unset'
          width='fit-content'
        />
        <Text fontWeight='semibold'>{skill.title}</Text>
      </Flex>
    </SkillContainer>
  );
};

const Skills = () => {
  return (
    <PageSection>
      <Heading as='h2' size='lg' marginBottom={8}>
        Skills
      </Heading>
      <Flex wrap='wrap' gap={4} overflow='hidden'>
        {SKILLS.map((skill, i) => (
          <Skill
            skill={skill}
            key={i}
            delay={(ANIM_TIME / SKILLS.length) * i}
          />
        ))}
      </Flex>
    </PageSection>
  );
};

export default Skills;
