import PageSection from '@/components/page-section';
import { Heading, VStack, Text } from '@chakra-ui/react';

const Bio = () => {
  return (
    <PageSection>
      <Heading as='h2' size='lg' marginBottom={8}>
        My Bio
      </Heading>
      <VStack align='stretch'>
        <Text>
          I'm a Computer Science Engineer who enjoys making video games with
          other people.
        </Text>
        <Text>
          I have a strong passion for continuous learning, tackling challenges,
          and constantly enhancing my skills. Additionally, I enjoy sharing the
          new things I learn with my colleagues.
        </Text>
        <Text>
          When I'm not in front of my computer, I'm most likely reading, playing
          video games or hiking in the mountains. ⛰️
        </Text>
        <Text>My favorite video game is Shenmue from SEGA Dreamcast.</Text>
      </VStack>
    </PageSection>
  );
};

export default Bio;
