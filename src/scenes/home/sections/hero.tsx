import { Box, Flex, Heading, Image, SimpleGrid } from '@chakra-ui/react';
import Typewriter from 'typewriter-effect';

const subtitles = [
  'Computer Science Engineer',
  'Video games developer',
  'BJJ white belt',
];

const Hero = () => {
  return (
    <Box
      as='section'
      height='100vh'
      display='flex'
      alignItems='center'
      overflow='hidden'
    >
      <SimpleGrid columns={{ base: 1, sm: 2 }} gap={8} width='full'>
        <Box maxWidth={{ base: 'xs', sm: 'unset' }} marginX='auto'>
          <Image src='images/eulalio.png' alt='Hero' />
        </Box>
        <Flex flexDirection='column' gap={4} justifyContent='center'>
          <Box height='full' display={{ base: 'none', sm: 'unset' }} />
          <Heading as='h1' size='2xl'>{`Hi I'm José`}</Heading>
          <Heading as='h2' size='lg' height='full'>
            <Typewriter
              options={{
                strings: subtitles,
                autoStart: true,
                loop: true,
                delay: 50,
              }}
            />
          </Heading>
        </Flex>
      </SimpleGrid>
    </Box>
  );
};

export default Hero;
