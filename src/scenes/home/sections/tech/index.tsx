import PageSection from '@/components/page-section';
import { Heading } from '@chakra-ui/react';
import TechCarousel from './carousel';

const Tech = () => {
  return (
    <PageSection>
      <Heading as='h2' size='lg' marginBottom={8}>
        A slice of my tech stack
      </Heading>
      <TechCarousel />
    </PageSection>
  );
};

export default Tech;
