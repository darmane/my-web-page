import { Flex, Image, Grid, GridItem, useColorMode } from '@chakra-ui/react';
import { useMemo } from 'react';
import styles from './styles.module.css';
import { techLogos } from '@/lib/tech-logos';

const LogosRow = ({
  reversed,
  logos,
}: {
  reversed?: boolean;
  logos: Array<string>;
}) => {
  return (
    <GridItem overflow='hidden'>
      <Flex
        width={{ base: '350%', md: '200%' }}
        gap={4}
        className={reversed ? styles['animate-reversed'] : styles.animate}
      >
        <Flex
          width='50%'
          gap={4}
          justifyContent='space-around'
          alignItems='center'
        >
          {logos.map((logo, i) => (
            <Image
              key={i}
              src={`/images/tech/${logo}`}
              alt={logo}
              width={{ base: 20, sm: 24 }}
              height='auto'
              fit='contain'
            />
          ))}
        </Flex>
        <Flex
          width='50%'
          gap={4}
          justifyContent='space-around'
          alignItems='center'
          overflow='auto'
        >
          {logos.map((logo, i) => (
            <Image
              key={i}
              src={`/images/tech/${logo}`}
              alt={logo}
              width={{ base: 20, sm: 24 }}
              height='auto'
              fit='contain'
              overflow='auto'
            />
          ))}
        </Flex>
      </Flex>
    </GridItem>
  );
};

const TechCarousel = () => {
  const { colorMode } = useColorMode();

  const logos = useMemo(() => {
    return colorMode === 'light' ? techLogos.light : techLogos.dark;
  }, [colorMode]);

  return (
    <Grid gridTemplateColumns='repeat(1, 1fr)' gap={4}>
      {[0, 1, 2].map(slice => (
        <LogosRow
          key={slice}
          logos={logos.slice(
            slice * (logos.length / 3),
            (slice + 1) * (logos.length / 3)
          )}
          reversed={slice % 2 == 0}
        />
      ))}
    </Grid>
  );
};

export default TechCarousel;
