import Link from '@/components/link';
import PageSection from '@/components/page-section';
import { Button, Heading, useColorModeValue } from '@chakra-ui/react';

const Contact = () => {
  return (
    <PageSection
      display='flex'
      flexDirection='column'
      alignItems='center'
      gap={8}
      bgColor={useColorModeValue('teal.50', 'teal.900')}
      padding={12}
      rounded='xl'
      boxShadow='md'
      border='solid'
      borderColor='teal.300'
    >
      <Heading as='h2' size='lg'>
        Sounds like fit?
      </Heading>
      <Link href='/contact'>
        <Button colorScheme='teal'>Then say hello! 👋</Button>
      </Link>
    </PageSection>
  );
};

export default Contact;
