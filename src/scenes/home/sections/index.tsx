export { default as Hero } from './hero';
export { default as Bio } from './bio';
export { default as Resume } from './resume';
export { default as Tech } from './tech';
export { default as Skills } from './skills';
export { default as Contact } from './contact';
