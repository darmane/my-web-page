import { NextPage } from 'next';
import AnimatedPage from '@/components/animated-page';
import { Hero, Bio, Resume, Tech, Skills, Contact } from './sections';

const Home: NextPage = () => {
  return (
    <AnimatedPage>
      <Hero />
      <Bio />
      <Resume />
      <Skills />
      <Tech />
      <Contact />
    </AnimatedPage>
  );
};

export default Home;
