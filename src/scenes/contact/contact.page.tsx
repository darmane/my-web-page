import AnimatedPage from '@/components/animated-page';
import Link from '@/components/link';
import PageSection from '@/components/page-section';
import {
  Button,
  Divider,
  Heading,
  Icon,
  Text,
  Image,
  Center,
} from '@chakra-ui/react';
import { NextPage } from 'next';
import { NextSeo, NextSeoProps } from 'next-seo';
import { IoIosMail } from 'react-icons/io';

const SEO: NextSeoProps = {
  title: 'Contact',
  description:
    "I'll love hearing from you! Write me, I will get back to you shortly.",
  canonical: 'https://futura64.com/contact',
};

const Contact: NextPage = () => {
  return (
    <>
      <NextSeo {...SEO} />
      <AnimatedPage marginTop={24}>
        <PageSection>
          <Heading
            as='h1'
            marginBottom={8}
          >{`I'll love hearing from you!`}</Heading>
          <Text alignSelf='start'>{`Write me, I will get back to you shortly.`}</Text>
          <Divider marginY={8} />
          <Center>
            <Link href='mailto:hello@futura64.com'>
              <Button
                colorScheme='teal'
                alignSelf='center'
                rightIcon={<Icon as={IoIosMail} />}
              >
                hello@futura64.com
              </Button>
            </Link>
          </Center>
        </PageSection>
        <PageSection display='flex' flexDirection='column' gap='4'>
          <Heading as='h2' size='md'>{`Where am I?`}</Heading>
          <Text alignSelf='center'>{`Currently I'm based in Seville, Spain (also knows as Naboo for the Star Wars fans).`}</Text>
          <Image
            alignSelf='center'
            width={{ base: 'full', sm: 'lg' }}
            src='images/sevilla.png'
            alt='Sevilla, la Giralda'
          />
        </PageSection>
      </AnimatedPage>
    </>
  );
};

export default Contact;
