import AnimatedPage from '@/components/animated-page';
import PageSection from '@/components/page-section';
import { Box, Heading, Image, Text } from '@chakra-ui/react';
import { NextPage } from 'next';
import { NextSeo, NextSeoProps } from 'next-seo';

const SEO: NextSeoProps = {
  title: 'Blog',
  description:
    'My blog. Here I write about development, videogames, entrepreneurship and other topics that interest me.',
  canonical: 'https://futura64.com/blog',
};

const Blog: NextPage = () => {
  return (
    <>
      <NextSeo {...SEO} />
      <AnimatedPage>
        <PageSection
          height='100vh'
          overflow='hidden'
          display='flex'
          flexDirection='column'
          justifyContent='center'
          textAlign='center'
          gap={12}
        >
          <Heading as='h1'>This site is still under construction...</Heading>
          <Text>Come back later!</Text>
          <Box marginX='auto'>
            <Image
              src='images/shenmueforklift.gif'
              alt='Ryo Hazuki working at the docks'
            />
          </Box>
        </PageSection>
      </AnimatedPage>
    </>
  );
};

export default Blog;
