import AnimatedPage from '@/components/animated-page';
import {
  Heading,
  ListItem,
  Text,
  UnorderedList,
  Flex,
  Link,
} from '@chakra-ui/react';
import { NextSeo, NextSeoProps } from 'next-seo';

const SEO: NextSeoProps = {
  title: 'Cookies',
  description:
    'Una cookie o galleta informática es un pequeño archivo de información que se guarda en su navegador cada vez que visita nuestra página web. Las cookies no suelen recoger categorías especiales de datos personales (datos sensibles). Los datos que guardan son de carácter técnico, preferencias personales, personalización de contenidos, etc.',
  canonical: 'https://confused.studio/cookies-policy',
};

const Cookies = () => {
  return (
    <>
      <NextSeo {...SEO} />
      <AnimatedPage>
        <Heading marginTop={100} marginBottom={8}>
          Política de cookies
        </Heading>
        <Flex flexDir='column' gap={4} textAlign='justify'>
          <Heading as='h2' size='md'>
            ¿Qué son las cookies y para qué las usamos?
          </Heading>
          <Text>
            Una cookie o galleta informática es un pequeño archivo de
            información que se guarda en su navegador cada vez que visita esta
            página web.
          </Text>
          <Text>
            La utilidad de las cookies es guardar el historial de su actividad
            en esta página web, de manera que, cuando la visite nuevamente, ésta
            pueda identificarle y configurar el contenido de la misma en base a
            sus hábitos de navegación, identidad y preferencias.
          </Text>
          <Text>
            Una cookie es inofensiva, no contiene código malicioso o
            malintencionado (ej. virus, troyanos, gusanos, etc.) que pueda dañar
            su terminal (ordenador, smartphone, tableta, etc.), pero sí tiene
            cierto impacto sobre su derecho a la protección de sus datos, pues
            recoge determinada información concerniente a su persona (hábitos de
            navegación, identidad, preferencias, etc.).
          </Text>
          <Heading as='h2' size='md'>
            ¿Qué información guarda una cookie?
          </Heading>
          <Text>
            Las cookies no suelen recoger categorías especiales de datos
            personales (datos sensibles). Los datos que guardan son de carácter
            técnico, preferencias personales, personalización de contenidos,
            etc.
          </Text>
          <Heading as='h2' size='md'>
            ¿Qué tipo de cookies existen?
          </Heading>
          <Text>Con carácter general, existen cinco tipos de cookies:</Text>
          <UnorderedList>
            <ListItem>
              <strong>Cookies técnicas: </strong>Son las cookies más básicas.
              Permiten al usuario la navegación a través de una página web,
              plataforma o aplicación y la utilización de las diferentes
              opciones o servicios que en ella existan como, por ejemplo,
              controlar el tráfico y la comunicación de datos, identificar la
              sesión, acceder a partes de acceso restringido, recordar los
              elementos que integran un pedido, realizar el proceso de compra de
              un pedido, realizar la solicitud de inscripción o participación en
              un evento, utilizar elementos de seguridad durante la navegación,
              almacenar contenidos para la difusión de videos o sonido o
              compartir contenidos a través de redes sociales.
            </ListItem>
            <ListItem>
              <strong>Cookies de personalización: </strong>Son aquéllas que
              permiten al usuario acceder al servicio con algunas
              características de carácter general predefinidas en función de una
              serie de criterios en el terminal del usuario como por ejemplo
              serian el idioma, el tipo de navegador a través del cual accede al
              servicio, la configuración regional desde donde accede al
              servicio, etc.
            </ListItem>
            <ListItem>
              <strong>Cookies de análisis: </strong> Son aquéllas que permiten
              al responsable de las mismas, el seguimiento y análisis del
              comportamiento de los usuarios de los sitios web a los que están
              vinculadas. La información recogida mediante este tipo de cookies
              se utiliza en la medición de la actividad de los sitios web,
              aplicación o plataforma y para la elaboración de perfiles de
              navegación de los usuarios de dichos sitios, aplicaciones y
              plataformas, con el fin de introducir mejoras en función del
              análisis de los datos de uso que hacen los usuarios del servicio.
            </ListItem>
            <ListItem>
              <strong>Cookies publicitarias: </strong> Son aquéllas que permiten
              la gestión, de la forma más eficaz posible, de los espacios
              publicitarios que, en su caso, el responsable haya incluido en una
              página web, aplicación o plataforma desde la que presta el
              servicio solicitado en base a criterios como el contenido editado
              o la frecuencia en la que se muestran los anuncios.
            </ListItem>
            <ListItem>
              <strong>Cookies de publicidad comportamental: </strong> Son
              aquéllas que permiten la gestión, de la forma más eficaz posible,
              de los espacios publicitarios que, en su caso, el responsable haya
              incluido en una página web, aplicación o plataforma desde la que
              presta el servicio solicitado. Estas cookies almacenan información
              del comportamiento de los usuarios obtenida a través de la
              observación continuada de sus hábitos de navegación, lo que
              permite desarrollar un perfil específico para mostrar publicidad
              en función del mismo.
            </ListItem>
          </UnorderedList>
          <Heading as='h2' size='md'>
            ¿Qué son las cookies propias y las de terceros?
          </Heading>
          <UnorderedList>
            <ListItem>
              <strong>Cookies propias: </strong> Las cookies propias son
              aquellas que se generan y gestionan por el propio responsable que
              presta el servicio solicitado por el usuario.
            </ListItem>
            <ListItem>
              <strong>Cookies de terceros: </strong> Son aquellas que se generan
              por otras entidades distintas al propio responsable (servicios o
              proveedores externos, como por ejemplo Google).
            </ListItem>
          </UnorderedList>
          <Heading as='h2' size='md'>
            ¿Qué tipo de cookies guarda esta página web?
          </Heading>
          <Text>
            A continuación, procedemos a relacionar el tipo de cookies que
            guarda esta página web y la finalidad de las mismas:
          </Text>
          <UnorderedList>
            <ListItem>
              <strong>chakra-ui-color-mode: </strong> esta cookie sirve para
              guardar tu selección de visualización de la página en &quot;modo
              claro&quot; o &quot;modo oscuro&quot;, de modo que se mantenga el
              modo seleccionado mientras navegas por nuestro sitio web.
            </ListItem>
            <ListItem>
              <strong>cookies-storage: </strong> esta cookie sirve para
              almacenar tu decisión de aceptar las cookies. Gracias a esta, no
              aparece de nuevo el aviso de las cookies cuando vuelves a entrar
              en esta web si ya la has visitado previamente.
            </ListItem>
          </UnorderedList>
          <Heading as='h2' size='md'>
            ¿Qué puedo hacer con las cookies?
          </Heading>
          <Text>
            Las cookies pueden ser borradas, aceptadas o bloqueadas, según
            desee, para esto sólo debe configurar convenientemente el navegador
            web.
          </Text>
          <Text>
            En cualquier momento, puede impedir la instalación de cookies
            (bloqueo) en su equipo mediante la opción correspondiente de su
            navegador, pero en dicho caso no podremos asegurarle el correcto
            funcionamiento de las distintas funcionalidades de esta página web.
          </Text>
          <Text>
            A continuación, le facilitamos los enlaces para la gestión y bloqueo
            de cookies dependiendo del navegador que utilice:
          </Text>
          <UnorderedList>
            <ListItem>
              <Link
                href='http://windows.microsoft.com/es-xl/internet-explorer/delete-manage-cookies#ie=ie-10'
                isExternal
                color='teal'
                fontWeight='bold'
              >
                Internet Explorer
              </Link>
            </ListItem>
            <ListItem>
              <Link
                href='http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we'
                isExternal
                color='teal'
                fontWeight='bold'
              >
                Firefox
              </Link>
            </ListItem>
            <ListItem>
              <Link
                href='https://support.google.com/chrome/answer/95647?hl=es'
                isExternal
                color='teal'
                fontWeight='bold'
              >
                Google Chrome
              </Link>
            </ListItem>
            <ListItem>
              <Link
                href='https://www.apple.com/legal/privacy/es/cookies/'
                isExternal
                color='teal'
                fontWeight='bold'
              >
                Safari
              </Link>
            </ListItem>
          </UnorderedList>
          <Text>
            También puede borrar las cookies que tenga guardadas en su navegador
            acudiendo a las opciones de configuración del mismo.
          </Text>
        </Flex>
      </AnimatedPage>
    </>
  );
};

export default Cookies;
