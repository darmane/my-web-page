export interface IMilestone {
  title: string;
  date: string;
  place: string;
  description?: string;
  skills?: string;
}

const resume: { education: IMilestone[]; experience: IMilestone[] } = {
  education: [
    {
      title: 'Master’s Degree in Computer Science',
      date: 'November 2020 - June 2021',
      place: 'ETSII - University of Seville (Spain)',
    },
    {
      title: 'University Degree in Computer Science',
      date: 'September 2016 - June 2020',
      place: 'ETSII - University of Seville (Spain)',
      description:
        'Co-Management of SERIOUS GAMES, student video game development group, and Organized workshops on video game development.',
    },
    {
      title: 'University Degree in Economics',
      date: 'September 2014 - June 2016',
      place: 'FCEYE - University of Seville (Spain)',
    },
  ],
  experience: [
    {
      title: 'Gameplay Engineer',
      date: 'Jul. 2023 - Present',
      place: 'Flying Wild Hog',
      skills: 'Unreal Engine, C++, P4.',
      description:
        'I take part in several unannounced high-quality projects, wearing multiple hats. My responsibilities span gameplay mechanics, systems design, dev tools for designers, UI, networking and code reviews.',
    },
    {
      title: 'Gameplay Engineer',
      date: 'Mar. 2021 - Jul. 2023',
      place: 'Freelancer',
      skills: 'Unreal Engine, C++, Node.js, Git, P4, Linux.',
      description:
        'Helped video game teams in a wide range of transversal areas, from gameplay systems to networking and Cloud infrastructure.',
    },
    {
      title: 'Gameplay Engineer (Contract)',
      date: 'Jun. 2022 - Sep. 2022',
      place: 'Blackmouth Games',
      skills: 'Unreal Engine, C++, Node.js, AWS, Git.',
      description:
        'Contributed to the development of a large online multiplayer third-person shooter. Successfully integrated two major areas of the project.',
    },
    {
      title: 'Software Engineer',
      date: 'Jul. 2020 - Sep. 2021',
      place: 'Cima Horizons',
      skills:
        'Unreal Engine, C++, Unity, C#, Node.js, Android, AWS, Git, Linux.',
      description:
        'Developed innovative AR and VR solutions, seamlessly integrating game engines, native mobile development, web technologies and Cloud computing.',
    },
    {
      title: 'Gameplay Programmer',
      date: 'Oct. 2018 - Dec. 2019',
      place: 'TieSoft',
      skills: 'Unreal Engine, C++, Unity, C#.',
      description: 'Worked on gameplay mechanics, UI and porting to PS4.',
    },
  ],
};

export default resume;
