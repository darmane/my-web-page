import { extendTheme, ThemeConfig } from '@chakra-ui/react';
import { mode } from '@chakra-ui/theme-tools';

const styles = {
  global: (props: any) => ({
    body: {
      bg: mode('white', '#202023')(props),
    },
  }),
};

const fonts = {
  heading: `"Anonymous Pro", monospace`,
  body: `"Anonymous Pro", monospace`
};

const config: ThemeConfig = {
  initialColorMode: 'dark',
};

export const theme = extendTheme({ styles, fonts, config });
