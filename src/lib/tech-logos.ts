export const techLogos: { light: string[]; dark: string[] } = {
  light: [
    'cpp.png',
    'csharp.png',
    'unreal.png',
    'typescript.png',
    'python.png',
    'kotlin.png',
    'bash.png',
    'haskell.png',
    'git.png',
    'postgresql.png',
    'mongodb.png',
    'nodejs.png',
    'aws.png',
    'docker.png',
    'linux.png',
  ],
  dark: [
    'cpp.png',
    'csharp.png',
    'unreal.png',
    'typescript.png',
    'python.png',
    'kotlin.png',
    'bash.png',
    'haskell.png',
    'git.png',
    'postgresql.png',
    'mongodb.png',
    'nodejs.png',
    'aws.png',
    'docker.png',
    'linux.png',
  ],
};
