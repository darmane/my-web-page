import { Text, Icon } from '@chakra-ui/react';
import { ImArrowRight2 } from 'react-icons/im';
import Link from '@/components/link';

const InlineLink = ({ children, href }: Props) => {
  return (
    <Text as='span' fontWeight='bold' _hover={{ textColor: 'teal' }}>
      <Link href={href}>{children}</Link>
      <Icon as={ImArrowRight2} verticalAlign='middle' />
    </Text>
  );
};

interface Props {
  children: string;
  href: string;
}

export default InlineLink;
