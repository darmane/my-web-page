import { Box, BoxProps } from '@chakra-ui/react';

const PageSection = ({ children, ...props }: BoxProps) => {
  return (
    <Box as='section' marginBottom={24} {...props}>
      {children}
    </Box>
  );
};

export default PageSection;
