import NextLink from 'next/link';

const Link = ({ children, href }: Props) => {
  return (
    <NextLink href={{ pathname: href }} passHref scroll={true}>
      {children}
    </NextLink>
  );
};

interface Props {
  children: React.ReactNode;
  href: string;
}

export default Link;
