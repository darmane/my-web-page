import {
  Container,
  Text,
  IconButton,
  HStack,
  Flex,
  Divider,
  Link,
} from '@chakra-ui/react';
import { useMemo } from 'react';
import { BsLinkedin } from 'react-icons/bs';
import { AiFillGitlab } from 'react-icons/ai';

const Footer = () => {
  const currentYear = useMemo(() => new Date().getFullYear(), []);

  return (
    <Container marginTop={12} maxWidth='container.md'>
      <Divider />
      <Flex
        flexDirection={{ base: 'column', sm: 'row' }}
        justifyContent='space-between'
        alignItems='center'
        gap={{ base: 4, sm: undefined }}
        paddingY={8}
      >
        <Text fontSize={'sm'} color='gray'>
          © {currentYear} - All rights reserved
        </Text>
        <HStack>
          <Link href='https://www.linkedin.com/in/jbtorres' isExternal>
            <IconButton
              rounded='full'
              size='sm'
              aria-label='LinkedIn'
              icon={<BsLinkedin />}
              colorScheme='linkedin'
            />
          </Link>
          <Link href='https://gitlab.com/users/darmane/projects' isExternal>
            <IconButton
              rounded='full'
              size='sm'
              aria-label='GitLab'
              icon={<AiFillGitlab />}
              colorScheme='orange'
            />
          </Link>
        </HStack>
      </Flex>
    </Container>
  );
};

export default Footer;
