import { useEffect, useRef } from 'react';
import {
  Box,
  BoxProps,
  Button,
  Container,
  IconButton,
  Link as ChakraLink,
  Stack,
  Text,
  useColorModeValue,
} from '@chakra-ui/react';
import { motion, useAnimation } from 'framer-motion';
import Link from '@/components/link';
import { BiCookie } from 'react-icons/bi';
import { useStore } from '../../store';

const MotionBox = motion<BoxProps>(Box);

const CookiesAlert = () => {
  const cookiesRef = useRef<HTMLDivElement>(null);

  const animation = useAnimation();

  const { accepted, accept: acceptCookies } = useStore(state => state);

  useEffect(() => {
    if (accepted) {
      animation.start({
        y: '100%',
        opacity: 0,
        transition: {
          type: 'linear',
          duration: 0.75,
        },
      });
      setTimeout(() => {
        cookiesRef.current && (cookiesRef.current.style.display = 'none');
      }, 1000);
    } else {
      animation.start({
        y: 0,
        opacity: 1,
        transition: {
          type: 'linear',
          duration: 0.75,
          delay: 1,
        },
      });
    }

    // eslint-disable-next-line
  }, [accepted]);

  return (
    <Container
      ref={cookiesRef}
      maxWidth='container.lg'
      position='fixed'
      bottom={0}
      left={0}
      right={0}
      zIndex='toast'
    >
      <MotionBox
        padding={8}
        marginBottom={4}
        bg={useColorModeValue('whiteAlpha.900', 'blackAlpha.900')}
        rounded='lg'
        boxShadow='lg'
        maxWidth='md'
        whileHover={{ scale: 1.01 }}
        initial={{ y: 100, opacity: 0 }}
        animate={animation}
      >
        <Stack spacing={4}>
          <IconButton
            icon={<BiCookie size={28} />}
            aria-label='Cookies'
            rounded='full'
            colorScheme='yellow'
            size='lg'
            _hover={{}}
            _active={{}}
            cursor='unset'
            width='fit-content'
          />
          <Text>
            This site uses cookies to improve your experience while browsing the
            website. By browsing the website, you agree to the cookie policy.
          </Text>
          <Link href='/cookies-policy'>
            <ChakraLink fontWeight='semibold'>
              Read the cookies policy
            </ChakraLink>
          </Link>
          <Button
            colorScheme='teal'
            width='fit-content'
            onClick={() => acceptCookies()}
          >
            {`I'm ok with that`}
          </Button>
        </Stack>
      </MotionBox>
    </Container>
  );
};

export default CookiesAlert;
