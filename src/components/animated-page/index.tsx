import { Container, ContainerProps } from '@chakra-ui/react';
import { motion, Variants } from 'framer-motion';

const variants: Variants = {
  hidden: { opacity: 0 },
  enter: { opacity: 1 },
  exit: { opacity: 0 },
};

const MotionContainer = motion<ContainerProps>(Container);

const AnimatedPage = ({ children, ...props }: Props) => {
  return (
    <MotionContainer
      as='main'
      maxWidth='container.md'
      initial='hidden'
      animate='enter'
      exit='exit'
      variants={variants}
      transition={{ duration: 0.3, type: 'linear' } as any}
      {...props}
    >
      {children}
    </MotionContainer>
  );
};

interface Props
  extends Omit<
    ContainerProps,
    'onAnimationStart' | 'onDragStart' | 'onDragEnd' | 'onDrag'
  > {}

export default AnimatedPage;
