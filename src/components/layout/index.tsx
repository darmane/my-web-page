import Navbar from '@/components/navbar';
import Footer from '@/components/footer';
import CookiesAlert from '../cookies-alert';

const Layout = ({ children }: Props) => {
  return (
    <>
      <Navbar />
      {children}
      <Footer />
      <CookiesAlert />
    </>
  );
};

interface Props {
  children: React.ReactNode;
}

export default Layout;
