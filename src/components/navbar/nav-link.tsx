import Link from '@/components/link';
import { Link as ChakraLink } from '@chakra-ui/react';
import { NextRouter, withRouter } from 'next/router';

const NavLink = ({ href, label, router: { pathname } }: Props) => {
  const active = href === pathname;
  return (
    <Link href={href}>
      <ChakraLink color={active ? 'teal.500' : undefined}>{label}</ChakraLink>
    </Link>
  );
};

interface Props {
  href: string;
  label: string;
  router: NextRouter;
}

export default withRouter(NavLink);
