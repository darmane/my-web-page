import {
  Box,
  Container,
  HStack,
  Icon,
  Image,
  useColorModeValue,
  Menu,
  MenuButton,
  IconButton,
  MenuList,
  MenuItem,
  Link as ChakraLink,
  Button,
} from '@chakra-ui/react';
import { GiIncomingRocket, GiHamburger } from 'react-icons/gi';
import { AiFillGitlab } from 'react-icons/ai';
import Link from '@/components/link';
import ToggleColorButton from './toggle-color-button';
import NavLink from './nav-link';

const Logo = () => {
  const logo = useColorModeValue<string, string>(
    'logo_light.png',
    'logo_dark.png'
  );

  return <Image src={logo} alt='Futura64' maxWidth={100} />;
};

const NavMenu = () => {
  return (
    <HStack spacing='4' display={{ base: 'none', sm: 'unset' }}>
      <NavLink href='/' label='Home' />
      <NavLink href='/blog' label='Blog' />
      <NavLink href='/contact' label='Contact' />
    </HStack>
  );
};

const MobileNavMenu = () => {
  return (
    <Box display={{ sm: 'none' }}>
      <Menu>
        <MenuButton
          as={IconButton}
          aria-label='Navigation Menu'
          icon={<Icon as={GiHamburger} width='5' height='5' />}
          variant='outline'
          colorScheme='gray'
        />
        <MenuList>
          <Link href='/'>
            <MenuItem>Home</MenuItem>
          </Link>
          <Link href='/blog'>
            <MenuItem>Blog</MenuItem>
          </Link>
          <Link href='/contact'>
            <MenuItem>Contact</MenuItem>
          </Link>
          <Box paddingX={2}>
            <SourceButton />
          </Box>
        </MenuList>
      </Menu>
    </Box>
  );
};

const SourceButton = (props: any) => {
  return (
    <ChakraLink
      as='a'
      href='https://gitlab.com/darmane/my-web-page'
      isExternal
      {...props}
    >
      <Button
        colorScheme='orange'
        variant='outline'
        rightIcon={<Icon as={AiFillGitlab} width={5} height={5} />}
      >
        Source
      </Button>
    </ChakraLink>
  );
};

const Navbar = () => {
  return (
    <Box
      bgColor={useColorModeValue('whiteAlpha.700', 'blackAlpha.700')}
      zIndex='sticky'
      position='fixed'
      top={0}
      left={0}
      right={0}
    >
      <Container
        display='flex'
        justifyContent='space-between'
        alignItems='center'
        maxWidth='container.lg'
        paddingY='2'
      >
        <Link href='/'>
          <HStack as='a' spacing='2'>
            <Icon as={GiIncomingRocket} width='8' height='8' />
            <Logo />
          </HStack>
        </Link>
        <HStack spacing='4'>
          <NavMenu />
          <SourceButton display={{ base: 'none', sm: 'unset' }} />
          <ToggleColorButton />
          <MobileNavMenu />
        </HStack>
      </Container>
    </Box>
  );
};

export default Navbar;
