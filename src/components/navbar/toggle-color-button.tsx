import {
  Icon,
  IconButton,
  useColorMode,
  useColorModeValue,
} from '@chakra-ui/react';
import { BsMoonFill, BsSunFill } from 'react-icons/bs';

const ToggleColorButton = () => {
  const { toggleColorMode } = useColorMode();

  return (
    <IconButton
      colorScheme={useColorModeValue('purple', 'orange')}
      icon={useColorModeValue(
        <Icon as={BsMoonFill} color='white' />,
        <Icon as={BsSunFill} />
      )}
      aria-label='Switch color mode'
      onClick={toggleColorMode}
    />
  );
};

export default ToggleColorButton;
