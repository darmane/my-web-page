# My website

**[www.futura64.com](https://futura64.com)**

## Stack
- Next.js: React framework.
- Chakra UI: Design and styles.
- Zustand: State management.
- Framer Motion: Animations.
- Typewriter: Typing animation in home's hero section.

## License
MIT License.

You can fork this project for free under the following conditions:

- Add a link to my website
- Don't use my name, my photo, my email or any of my personal data.
