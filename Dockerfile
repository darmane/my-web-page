FROM amazonlinux:2

# This Dockerfile is of a custom image to use Amplify with git-lfs
# Thanks to Slawek Kolodziej -> https://github.com/slawekkolodziej/aws-amplify-lfs/blob/master/Dockerfile
# https://www.youtube.com/watch?v=TXqExf1Ta6M

# Framework Versions
ENV VERSION_NODE=14

# UTF-8 Environment
ENV LANGUAGE en_US:en
ENV LANG=en_US.UTF-8
ENV LC_ALL en_US.UTF-8

# Install OS packages
RUN touch ~/.bashrc
RUN yum -y update && \
    yum -y install \
        curl \
        git \
        openssl-devel \
        openssh-clients \
        openssh-server \
        openssh-clients \
        gcc-c++ \
        make

# Install Git-LFS
RUN curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.rpm.sh | bash && \
    yum -y install git-lfs

# Install NodeJS
RUN curl -sL https://rpm.nodesource.com/setup_${VERSION_NODE}.x | bash - && \
    yum -y install nodejs

# Check node installation
RUN node -v

# Clean installation
RUN yum clean all && \
    rm -rf /var/cache/yum

ENTRYPOINT [ "bash", "-c" ]
